import { AdminUser, Resolver } from "../types";
import { PubSub, withFilter } from "apollo-server-express";

/* 
parent: parent 객체. 거의 사용X
args: Query에 필요한 필드에 제공되는 인수(parameter)
context: 로그인한 사용자. DB Access 등의 중요한 정보들
*/

const pubsub = new PubSub();

const adminUserResolver: any = {
  Query: {
    adminUser: async (parent, { id }, { db }) => {
      const docRef = await db.adminUser.doc(id).get();
      return docRef.data();
    },
    adminUsers: async (parent, {}, { db }) => {
      const adminUsers: AdminUser[] = [];
      const snapshot = await db.adminUser.get();
      snapshot.forEach((adminUser) =>
        adminUsers.push({ id: adminUser.id, ...adminUser.data() })
      );
      return adminUsers;
    },
  },
  Mutation: {
    addAdminUser: async (parent, { email, name, subname }, { db }) => {
      const newRef = await db.adminUser.doc();
      const user = {
        id: newRef.id,
        email,
        name,
        subname,
        role: "CC",
        isApproved: false,
        uid: "",
      };
      await newRef.set(user);
      return user;
    },
    editAdminUser: async (parent, { id, ...args }, { db }) => {
      await db.adminUser.doc(id).update({ ...args });
      return { id: id, ...args };
    },
    deleteAdminUser: async (parent, { id }, { db }) => {
      await db.adminUser.doc(id).delete();
      return id;
    },
  },
  Subscription: {
    adminUserAdded: {
      subscribe: () => pubsub.asyncIterator("adminUserAdded"),
    },
    adminUserUpdated: {
      subscribe: withFilter(
        () => pubsub.asyncIterator("adminUserUpdated"),
        (payload, variables) => {
          return payload.adminUserUpdated.id === variables.id;
        }
      ),
    },
  },
};

export const publishAdminUserAdded = (users: AdminUser[]) => {
  pubsub.publish("adminUserAdded", {
    adminUserAdded: users,
  });
};

export const publishAdminUserUpdated = (users: AdminUser) => {
  pubsub.publish("adminUserUpdated", {
    adminUserUpdated: users,
  });
};

// setInterval(() => {
//   pubsub.publish("adminUserAdded", {
//     adminUserAdded: {
//       id: "sss",
//       email: "sally@tag-hive.com",
//       isApproved: "sdf",
//       name: "s",
//       role: "",
//       subname: "",
//       uid: "",
//     },
//   });
// }, 1000);

export default adminUserResolver;
