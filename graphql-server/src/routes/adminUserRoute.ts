import { AdminUser } from "./../types";
import { publishAdminUserAdded, publishAdminUserUpdated } from "../resolvers/adminUserResolver";
import dataStore from "../../firebaseConfig";

const adminUserRoute = [
  {
    // Create admin_user
    method: "post",
    route: "/adminUser",
    handler: async ({ body }, res) => {
      const { email, name, subname } = body;
      const adminUserRef = dataStore.collection("admin_users");
      const newDoc = await adminUserRef.doc();
      const user: AdminUser = {
        email,
        name,
        subname,
        role: "CC",
        isApproved: false,
        uid: "",
      };
      await newDoc.set(user);
      user.id = newDoc.id;
      const adminUsers: AdminUser[] = [];
      const snapshot = await adminUserRef.get();
      snapshot.forEach((adminUser) =>
        adminUsers.push({ id: adminUser.id, ...adminUser.data() })
      );
      publishAdminUserAdded(adminUsers);
      res.send(user);
    },
  },
  {
    // Update admin_user
    method: "put",
    route: "/adminUser",
    handler: async ({ body }, res) => {
      const { id, name, subname } = body;
      const adminUserRef = dataStore.collection("admin_users");
      await adminUserRef.doc(id).update({
        name,
        subname
      })
      const userDoc = await adminUserRef.doc(id).get();
      const user = userDoc.data() as AdminUser;
      user.id = userDoc.id;
      publishAdminUserUpdated(user);
      res.send(user);
    },
  },
];

export default adminUserRoute;
