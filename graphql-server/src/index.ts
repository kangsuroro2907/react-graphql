import express from "express";
import { ApolloServer } from "apollo-server-express";
import cors from "cors";
import typeDefs from "./schema";
import resolvers from "./resolvers";
import db from "../firebaseConfig";
import { createServer } from "http";
import adminUsersRoute from './routes/adminUserRoute';

const PORT = process.env.PORT || 5000;
const app = express();

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.use(
  cors({
    origin: "http://localhost:3000",
    credentials: true,
  })
);

const routes = [...adminUsersRoute];

routes.forEach(({ method, route, handler }) => {
  app[method](route, handler);
});

const server = new ApolloServer({
  typeDefs,
  resolvers,
  context: {
    db: {
      adminUser: db.collection("admin_users"),
    },
  },
});

server.applyMiddleware({ app, path: "/graphql" });

const httpServer = createServer(app);

server.installSubscriptionHandlers(httpServer);

httpServer.listen({ port: PORT }, () => {
  console.log(`server ready at http://localhost:${PORT}${server.graphqlPath}`);
  console.log(
    `Subscriptions ready at ws://localhost:${PORT}${server.subscriptionsPath}`
  );
});

// const { graphqlHTTP } = require('express-graphql')
// const { root } = require('./resolvers/Resolvers')

// app.listen(PORT, () => console.log(`Open ${PORT}/graphql`));

// await server.start();

// await app.listen({ port: PORT });
// console.log("server listening on 5000...");

// app.use('/graphql', graphqlHTTP({
//   schema: schema,
//   rootValue: root,
//   graphiql: true,
// }));