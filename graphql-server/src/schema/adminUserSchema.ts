import { gql } from "apollo-server-express";
const adminUserSchema = gql`
  type Query {
    adminUser(id : ID!) : AdminUser!
    adminUsers : [AdminUser]!
  }
  type Mutation {
    addAdminUser(email: String!, name: String, subname: String) : AdminUser!
    editAdminUser(id: ID!, email: String, name : String) : SemiAdminUser
    deleteAdminUser(id: ID!) : ID!
  }
  type Subscription {
    adminUserAdded: [AdminUser]!
    adminUserUpdated(id: ID!): AdminUser!
  }
  type AdminUser {
    id: ID!
    email: String!
    isApproved: Boolean
    name: String
    role: String
    subname: String
    uid: String
  }
  type SemiAdminUser{
    id: ID
    email : String
    name : String
  }
`;

export default adminUserSchema;