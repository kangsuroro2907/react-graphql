export interface Resolver {
  [key: string]: {
    [key: string]: (
      parent: any,
      variables: { [key: string]: any },
      context: {
        db: {
          adminUser: any
        };
      }
    ) => any;
  };
}

export interface AdminUser {
  id?: string;
  email?: string;
  isApproved?: boolean;
  name?: string;
  role?: string;
  subname?: string;
  uid?: string;
}
