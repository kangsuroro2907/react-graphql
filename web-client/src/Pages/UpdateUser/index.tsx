import React from "react";
import UpdateUserInputBox from "../../components/UpdateUserInputBox";
import "./index.css";

interface Props {}

const UpdateUser: React.FC<Props> = () => {
  return (
    <div className="UpdateUser">
      <UpdateUserInputBox />
    </div>
  );
};

export default UpdateUser;
