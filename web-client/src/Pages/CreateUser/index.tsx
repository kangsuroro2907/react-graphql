import React from 'react';
import AddUserInputBox from '../../components/AddUserInputBox';
import AdminUserList from '../../components/AdminUserList';
import './index.css';

interface Props {

}

const CreateUser: React.FC<Props> = () => {
  return (
    <div className="CreateUser">
      <AddUserInputBox />
      <AdminUserList />
    </div>
  );
}

export default CreateUser;
