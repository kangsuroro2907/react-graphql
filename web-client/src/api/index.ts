import axios from "axios";
import { UserInputs } from "../components/AddUserInputBox";

const baseURL = "http://localhost:5000";
const api = axios.create({ baseURL });

export const postAdminUser = (data: UserInputs) => {
  return api.post("/adminUser", data);
};

export const updateAdminUser = (data: Partial<UserInputs>) => {
  return api.put("/adminUser", data);
};
