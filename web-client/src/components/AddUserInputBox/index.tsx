import React, { useCallback, useState } from "react";
import { postAdminUser } from "../../api";
import UpdatedUserName from "../UpdatedUserName";
import "./index.css";

interface Props {}

export type UserInputs = {
  email: string;
  name: string;
  subname: string;
};

const AddUserInputBox: React.FC<Props> = () => {
  const [inputs, setInputs] = useState<UserInputs>({
    email: "",
    name: "",
    subname: "",
  });
  const [userId, setUserId] = useState<string>("");

  const onChange = useCallback((e: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    setInputs((inputs: UserInputs) => ({
      ...inputs,
      [name]: value,
    }));
  }, []);

  const onSubmit = useCallback(
    async (event: React.FormEvent<HTMLFormElement>) => {
      event.preventDefault();
      const { data } = await postAdminUser(inputs);
      setUserId(data.id);
    },
    [inputs]
  );

  return (
    <div className="AddUserInputBox">
      <form onSubmit={onSubmit}>
        <label htmlFor="email">EMAIL</label>
        <br />
        <input
          id="email"
          name="email"
          type="email"
          value={inputs.email}
          onChange={onChange}
        />
        <br />
        <label htmlFor="name">NAME</label>
        <br />
        <input
          id="name"
          name="name"
          type="text"
          value={inputs.name}
          onChange={onChange}
        />
        <br />
        <label htmlFor="subname">SUBNAME</label>
        <br />
        <input
          id="subname"
          name="subname"
          type="text"
          value={inputs.subname}
          onChange={onChange}
        />
        <br />
        <button>submit</button>
      </form>
      {userId && <UpdatedUserName userId={userId} />}
    </div>
  );
};

export default AddUserInputBox;
