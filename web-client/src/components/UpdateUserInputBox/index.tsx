import React, { useCallback, useState } from "react";
import { updateAdminUser } from "../../api";
import "./index.css";

interface Props {}

export type UserInputs = {
  id: string;
  name: string;
  subname: string;
};

const UpdateUserInputBox: React.FC<Props> = () => {
  const [inputs, setInputs] = useState<UserInputs>({
    id: "",
    name: "",
    subname: "",
  });

  const onChange = useCallback((e: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    setInputs((inputs: UserInputs) => ({
      ...inputs,
      [name]: value,
    }));
  }, []);

  const onSubmit = useCallback(
    async (event: React.FormEvent<HTMLFormElement>) => {
      event.preventDefault();
      const { data } = await updateAdminUser(inputs);
    },
    [inputs]
  );

  return (
    <div className="UpdateUserInputBox">
      <form onSubmit={onSubmit}>
        <label htmlFor="id">ID</label>
        <br />
        <input
          id="id"
          name="id"
          type="text"
          value={inputs.id}
          onChange={onChange}
        />
        <br />
        <label htmlFor="name">NAME</label>
        <br />
        <input
          id="name"
          name="name"
          type="text"
          value={inputs.name}
          onChange={onChange}
        />
        <br />
        <label htmlFor="subname">SUBNAME</label>
        <br />
        <input
          id="subname"
          name="subname"
          type="text"
          value={inputs.subname}
          onChange={onChange}
        />
        <br />
        <button>submit</button>
      </form>
    </div>
  );
};

export default UpdateUserInputBox;
