import React from "react";
import "./index.css";
import gql from "graphql-tag";
import { useSubscription } from "@apollo/react-hooks";

interface Props {}

const ADMINUSER_SUBSCRIPTION = gql`
  subscription adminUserAdded {
    adminUserAdded {
      email
      name
    }
  }
`;

const AdminUserList: React.FC<Props> = () => {
  const { loading, error, data } = useSubscription(ADMINUSER_SUBSCRIPTION);
  if (loading) return <p>Subscribing...</p>;
  if (error) return <p>Error!</p>;
  const { adminUserAdded } = data;
  return (
    <div className="AdminUserList">
      <h1>userList</h1>
      {adminUserAdded?.map(({ email, name }: any, index: number) => (
        <p key={index}>
          <span>{email}</span>
        </p>
      ))}
    </div>
  );
};

export default AdminUserList;
