import React from "react";
import "./index.css";
import gql from "graphql-tag";
import { useSubscription } from "@apollo/react-hooks";

interface Props {
  userId: string;
}

const ADMINUSER_SUBSCRIPTION_UPDATED = gql`
  subscription adminUserUpdated($id: ID!) {
    adminUserUpdated(id: $id) {
      name
      subname
    }
  }
`;

const UpdatedUserName: React.FC<Props> = ({ userId }) => {
  const { loading, error, data } = useSubscription(ADMINUSER_SUBSCRIPTION_UPDATED, {
    variables: { id: userId },
  });
  if (loading) return <p>Subscribing...</p>;
  if (error) return <p>Error!</p>;
  const { adminUserUpdated } = data;
  return <div className="UpdatedUserName">updated name: {adminUserUpdated.name}</div>;
};

export default UpdatedUserName;
