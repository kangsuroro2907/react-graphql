import React from "react";
import "./App.css";
import { client } from "../graphqlConfig";
import { ApolloProvider } from "@apollo/client";
import { Route, Router } from "react-router-dom";
import * as Pages from "../Pages";
import { createBrowserHistory } from 'history';


const history = createBrowserHistory();

function App() {
  return (
    <ApolloProvider client={client}>
      <Router history={history}>
        <Route exact path="/create" component={Pages.CreateUser} />
        <Route exact path="/update" component={Pages.UpdateUser} />
      </Router>
    </ApolloProvider>
  );
}

export default App;
