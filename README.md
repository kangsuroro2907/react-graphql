# README

## Server 

### default port: 5000

```
cd graphql-server
npm install
npm start
```
- must add serviceAccountKey.json (on dev)

## Client

### default port: 3000

```
cd web-client
npm install
npm start
```

### Route
- /create : create new user
- /update : update user'name with admin_user collection id


